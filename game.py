class Colors:
    RED = "\u001b[31;1m"
    BLUE = "\u001b[34;1m"
    GREEN = "\u001b[32;1m"
    WHITE = "\u001b[37;1m"


class CurrentFrame:
    grid = [[0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0]]

    def get_free_stone(self, column: int):
        for row in range(0, 6):
            try:
                if self.grid[row+1][column] is 0:
                    continue
                else:
                    return row
            except IndexError:
                return 5

    def drop_stone(self, side: int, column: int):
        row = self.get_free_stone(self=self, column=column)
        self.grid[row][column] = side
        return


def draw_frame(frame: CurrentFrame):
    ttp = ""
    for row in frame.grid:
        for column in row:
            if column == 0:
                ttp += "[ ]"
            elif column == 1:
                ttp += f"[{Colors.RED}\u2B1B{Colors.WHITE}]"
            elif column == 2:
                ttp += f"[{Colors.BLUE}\u2B1B{Colors.WHITE}]"
            else:
                return print(f"{Colors.RED}Uhh... I'mma keep it 100 with you chief, this wasn't supposed to happen...")
        ttp += "\n"
    print(ttp)


def get_input(player):
    if player == 1:
        it = f"Its {Colors.RED}RED{Colors.WHITE}'s Turn, choose a row from 1 to 9: "
    elif player == 2:
        it = f"Its {Colors.BLUE}BLUE{Colors.WHITE}'s Turn, choose a row from 1 to 9: "
    else:
        return print(f"{Colors.RED}Uhh... I'mma keep it 100 with you chief, this wasn't supposed to happen...")
    itsb = input(it)
    try:
        if int(itsb) in range(1, 10):
            return int(itsb)-1
        else:
            return get_input(player)
    except:
        return get_input(player)


def main_loop(frame, last_player):
    if last_player == 1:
        player = 2
    elif last_player == 2:
        player = 1
    else:
        return print(f"{Colors.RED}Uhh... I'mma keep it 100 with you chief, this wasn't supposed to happen...")
    ig = get_input(player)
    frame.drop_stone(self=frame, side=player, column=int(ig))
    draw_frame(frame)
    main_loop(frame, player)


if __name__ == "__main__":
    frame = CurrentFrame
    main_loop(frame, 2)
